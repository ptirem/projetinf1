package lucioles;

// Étape 1 : Simulation d'une seule luciole

public class LucioleSeule {

	// Seuil au delà duquel une luciole émet un fl
	
	public static final double SEUIL = 100.0;
	/**
	 * 
	 * @param niveauEnergie : entier
	 * @return le caractere . si < au seuil sinon return * 
	 */
	public static char symboliseLuciole(double niveauEnergie) {
		if (niveauEnergie<SEUIL) {
			return '.';
		}
		return '*';
	}
	/**
	 * affiche une luciole
	 * @param niveauxEnergie :int
	 * @param verbeux boolean si il faut afficher le niveaux d'energie en plus 
	 */
	public static void afficheLuciole(double niveauxEnergie,boolean verbeux) {
		if(verbeux) {
			System.out.print(symboliseLuciole(niveauxEnergie)+niveauxEnergie);
		}
		else {
			System.out.print(symboliseLuciole(niveauxEnergie));
		}
		}
	
	public static void main(String[] args) {
		// TODO À compléter
		double lucioleEnergie=RandomGen.rGen.nextDouble()*100;
		
		afficheLuciole(lucioleEnergie, false);
		lucioleEnergie+=45;
		
		afficheLuciole(lucioleEnergie, false);
		lucioleEnergie+=10;
		
		afficheLuciole(lucioleEnergie, false);
		lucioleEnergie-=35;
		
		afficheLuciole(lucioleEnergie, false);
		lucioleEnergie+=45;
		
		afficheLuciole(lucioleEnergie, false);
		
		double LucioleDeltaEnergie = RandomGen.rGen.nextDouble();
			
	}

}
