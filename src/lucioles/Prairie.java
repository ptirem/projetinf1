package lucioles;

import outils.*;

// Étapes 2 et 3 : Définition de prairies, et simulation sans interaction

public class Prairie {

	// Seuil au delà duquel une luciole émet un flash.
	public static final double SEUIL = 100;

	// Indices nommés pour accéder aux données d'une luciole
	public static final int ENERGIE = 0;
	public static final int DELTA = 1;
	
	public static double[] creerLuciole() {
		
		double[] luciole = new double[2];
		luciole[ENERGIE] = RandomGen.rGen.nextDouble()*100;
		luciole[DELTA] = RandomGen.rGen.nextDouble();
		
		return luciole;
		
	}
	
	public static void incrementeLuciole(double[] luciole) {
		
		luciole[ENERGIE] += luciole[DELTA];
		
	}
	
	public static double[][] creerPopulation (int nbLucioles){
		
		double[][] population = new double[nbLucioles][2];
		
		for (int i = 0 ; i < population.length ; i++) {
			
			population[i] = creerLuciole();
			
			//Affichage pour vérification
			
			//System.out.println(population[i][0]);
			//System.out.println(population[i][1]);
			
		}
		
		return population;
		
	}
	
	public static int[][] prairieVide (int nbLignes, int nbColonnes){
		
		int[][] prairie = new int[nbLignes][nbColonnes];
		
		//On remplit la prairie de -1, signifiant que chaque case est vide
		
		for (int lignes = 0 ; lignes < prairie.length ; lignes++) {
			
			for (int col = 0 ; col < prairie[0].length ; col++) {
				
				prairie[lignes][col] = -1;
				
			}
			
		}
		
		return prairie;
		
	}
	
	public static void affichePrairie (int[][] prairie, double[][] population) {
		
		//La prairie doit être fermée
		//Alors on ajoute des bordures en haut et en bas
		//Ici on commence par le haut
		//Pour le bas, la boucle FOR se situe en dessous celle dédiée à l'intérieur de la prairie
		
		for (int bordures_haut = 0 ; bordures_haut < prairie[0].length + 2 ; bordures_haut++) {
			System.out.print("#");	
		}
		//L'intérieur de la prairie
		for (int lignes = 0 ; lignes < prairie.length ; lignes++) {
			//On affiche la première bordure
			System.out.println();
			System.out.print("#");
			for (int col = 0 ; col < prairie[0].length ; col++) {
				//Si la prairie est vide à cet emplacement, on affiche un espace
				if (prairie[lignes][col] ==  -1) {
					System.out.print(" ");
				}
				//Sinon, si l'énergie de la luciole se trouvant à cet emplacement est inférieur au seuil
				//On affiche un point
				else if (population[prairie[lignes][col]][ENERGIE] < SEUIL ) {
					System.out.print(".");
				}
				//Dans le cas où l'énergie de la luciole est supérieure au seuil
				//On affiche une étoile pour simuler un flash
				
				else {
					
					System.out.print("*");
					
				}
				
			}
			
			//On affiche la deuxième bordure
			
			System.out.print("#");
			
		}
		
		System.out.println();
		
		//On affiche les bordures du bas
		
		for (int bordures_bas = 0 ; bordures_bas < prairie[0].length + 2 ; bordures_bas++) {
			
			System.out.print("#");
			
		}
		
	}
	
	public static int[][] prairieLucioles (int nbLignes, int nbColonnes, double[][] population){
		
		//Initialisation de la prairie
		
		int[][] prairie = prairieVide(nbLignes, nbColonnes);
		
		//Remplissage de la prairie en fonction du nombre de lucioles
		//La prairie n'est constituée que de la position des lucioles dans la population
		//Les cases de la prairie non complétée restent vides, soient égales à -1
		
		//La première boucle FOR se répète tant que toutes les lucioles ne sont pas placées
		//La deuxième boucle DO WHILE vérifie qu'on ne place pas deux lucioles au même endroit
		
		for (int i = 0 ; i < population.length ; i++) {
			
			boolean place = false;
			
			do {
				
				int randomLignes = RandomGen.rGen.nextInt(nbLignes);
				int randomColonnes = RandomGen.rGen.nextInt(nbColonnes);
				
				if (prairie[randomLignes][randomColonnes] == -1) {
					
					prairie[randomLignes][randomColonnes] = i;
					place = true;
					
				}
				
			} while (!place);
					
		}
		
		return prairie;
		
	}
	
	public static void simulationPrairie (int[][] prairie, double[][] population, int pas) {
		
		for (int i = 0 ; i < pas ; i++) {
			
			affichePrairie(prairie, population);
			
			for (int j = 0 ; j < population.length ; j ++) {
				
				//Si l'énergie de la luciole dépasse le seuil
				//On réinitialise son énergie à 0
				
				if (population[j][ENERGIE] > SEUIL) {
					population[j][ENERGIE] = 0;
				}
				
				//Sinon on incrémente son énergie de delta
				
				else {
					incrementeLuciole(population[j]);
				}
				
			}
			
			System.out.println();
			System.out.println();
			
		}
		
	}
	
	public static void simulationPrairieGIF (int pas, double[][] population, int[][] prairie) {
		
		String[] images = new String[pas];
		double seuil = SEUIL;
		
		for (int i = 0 ; i < pas ; i++) {
			
			String nomFichier = "PROJET-LUCIOLES/projetinf1/img/image" + (i+1) + ".bmp";
			BitMap.bmpEcritureFichier(nomFichier, prairie, population, seuil);
			images[i] = nomFichier;
			
			for (int j = 0 ; j < population.length ; j++) {
				
				if (population[j][ENERGIE] >= SEUIL) {
					population[j][ENERGIE] = 0;
				}
				
				else {
					incrementeLuciole(population[j]);
				}
				
			}
			
		}
		
		GifCreator.construitGIF("PROJET-LUCIOLES/projetinf1/simu/prairieLuciolesGIF.gif", images);
		
	}

	public static void main(String[] args) {
		// TODO À compléter
		creerPopulation(3);
		
		System.out.println();
		
		int[][] prairie1 = {{0,1},{2,3},{4,-1}};
		double[][] population1 = creerPopulation(55);
		
		affichePrairie(prairie1, population1);
		
		System.out.println();
		System.out.println();
		
		int[][] prairie2 = {{0,1,2},{3,4,-1}};
		double[][] population2 = {{100,0.1},{101,0.1},{1000,0.5},{0,0.5},{50,0.5}};
		
		affichePrairie(prairie2, population2);
		
		System.out.println();
		System.out.println();
		
		int[][] prairie3 = prairieLucioles(30,30,population1);
	
			
			
		
		
		System.out.println();
		
		System.out.println("Affichage 1ere prairie :");
		System.out.println();
		simulationPrairie(prairie3, population1, 3);
		
		System.out.println("Affichage 2eme prairie :");
		System.out.println();
		simulationPrairie(prairie2, population2, 3);
		
		//double[][] population = creerPopulation(1000);
		//simulationPrairieGIF(100, population, prairieLucioles(500, 500, population));
		
	}

}
