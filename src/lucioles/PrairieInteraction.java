package lucioles;

import outils.*;

// Étape 4: Simulation d'une prairie avec interaction entre les lucioles

public class PrairieInteraction {

	// Seuil au delà duquel une luciole émet un flash.
	public static final double SEUIL = 100;

	// Indices nommés pour accéder aux données d'une luciole
	public static final int ENERGIE = 0;
	public static final int DELTA = 1;

	// Définition de l'apport d'énergie par flash, et du rayon de voisinage
	public static final double APPORT = 50.0;
	public static final int RAYON = 2;
	
	/**
	 * 
	 * @param t prairie de luciole
	 * @param l numero de la ligne 
	 * @param c numero de colonne
	 * @return un tableau d'entier de tout les voisin au rayon choisi
	 */
	public static int[] regarder_autour(int[][] t,int l,int c) {
		int[] tr=new int[(2*RAYON+1)*(2*RAYON+1)-1];
		for(int i=0;i<tr.length;i++) {
			tr[i]=-1;
		}	
		int indice=0;
		for(int i=-RAYON;i<=RAYON;i++) {
			for(int j=-RAYON;j<=RAYON;j++) {
				
				if(l+i>=0 && c+j>=0 && (j!=0 || i!=0) && l+i<t.length && c+j <t[i+l].length ){
				
					if(t[1][1]==0){
						
						tr[indice]=t[i+l][j+c];
						indice++;
					}
				}
	
			
			}
		}
		return tr;
	}
	
	/**
	 * 
	 * @param prairie
	 * @return le nombre de luciole dans la prairie 
	 */
	public static int nbluciole(int[][] prairie) {
		int r=0;
		for(int i=0;i<prairie.length;i++) {
			for(int j=0;j<prairie[i].length;j++) {
				if(prairie[i][j]!=-1) {
					r++;
				}
			}
		
		}
		return r;
	}
	/**
	 * 
	 * @param prairie
	 * @return tableau 2d d'entier qui poure chaque luciole renvoie ses voisins
	 */
	
	public static int[][] voisinage(int[][] prairie){
		int nb=nbluciole(prairie);
		int[][] tr=new int[nb][];
		for(int i=0;i<prairie.length;i++) {
			for(int j=0;j<prairie[i].length;j++) {
				if(prairie[i][j]!=-1) {
					tr[prairie[i][j]]=regarder_autour(prairie,i,j);
				}	
			}
		}
		return tr;
	
	}
	
	public static int[][] voisinage_test_q26(int[][] prairie){
		//to do
		int nb=nbluciole(prairie);
		int[][] tr=new int[nb][];
		for(int i=0;i<prairie.length;i++) {
			for(int j=0;j<prairie[i].length;j++) {
				if(prairie[i][j]!=-1) {
					tr[prairie[i][j]]=regarder_autour(prairie,i,j);
				}	
			}
		}
		return tr;
	
	}
	
	
	
	
	/**
	 * copie la population pop1 dans pop2
	 * @param pop1
	 * @param pop2
	 */
	public static void copiepop(double[][] pop1 , double[][] pop2){
		for(int i=0;i<pop1.length;i++){
			pop2[0]=pop1[0];
			pop2[1]=pop1[1];
		}
		
		
	}
	
	
	
	
		/**
		 * incremente une luciole par aport a sont niveaux d'energie d'avant et a ses voisine 
		 * @param population
		 * @param voisinage
		 * @param luciole
		 * @param numluciole
		 */
	public static void incrementLuciole(double[][] population,int[][] voisinage,double[] luciole,int numluciole) {
		
			if(luciole[ENERGIE]>SEUIL) {luciole[ENERGIE]=0;}
			Prairie.incrementeLuciole(luciole);
		
			for(int i=0;i<voisinage[numluciole].length;i++) {
			
			if(voisinage[numluciole][i]!=-1 && population[voisinage[numluciole][i]][0]>=SEUIL) {
				luciole[ENERGIE]+=APPORT;
				
			}
		}
	}	
	
	
	/**
	 * fait avancer d'un pas la population
	 * @param population
	 * @param voisinage
	 * @param population2
	 */
	public static void tour(double[][] population,int[][] voisinage,double[][] population2) {
		for(int i=0;i<population.length;i++) {
			incrementLuciole(population,voisinage,population[i],i);
		}
		copiepop(population2,population);
	}
	
	
	
	
	/**
	 * simule une prairie pendant le nb de pas specifier 
	 * @param prairie
	 * @param pop
	 * @param nb_pas
	 */
public static void simulationPrairie(int[][] prairie, double[][] pop,int nb_pas) {
		
		int[][] voi=voisinage(prairie);
		double[][] energie_suivant=new double[pop.length][2];
		copiepop(pop,energie_suivant);
		for(int i=0;i<nb_pas;i++) {
			tour(pop,voi,energie_suivant);
			Prairie.affichePrairie(prairie, pop);
		}
	
	}








	/**
	 * simule une prairie pendant le nb de pas specifier et rend un gif 
	 * @param pas
	 * @param population
	 * @param prairie
	 */
public static void simulationPrairieGIF (int pas, double[][] population, int[][] prairie) {
	
	String[] images = new String[pas];
	double seuil = SEUIL;
	int[][] voi=voisinage(prairie);
	double[][] energie_suivant=new double[population.length][2];
	copiepop(population,energie_suivant);
	for (int i = 0 ; i < pas ; i++) {
		
		String nomFichier = "/home/ptirem/Documents/projet/projetinf1/img/image" + (i+1) + ".bmp";
		BitMap.bmpEcritureFichier(nomFichier, prairie, population, seuil);
		images[i] = nomFichier;
		tour(population,voi,energie_suivant);
		
		
		
	}
	
	GifCreator.construitGIF("/home/ptirem/Documents/projet/projetinf1/simu/prairieLuciolesGIF.gif", images);
	
}




	
	
	
	
	
	public static void main(String[] args) {
		
		
		double[][] pop=Prairie.creerPopulation(300*300);
		int[][] prairie=Prairie.prairieLucioles(300, 300, pop);
		simulationPrairieGIF(10000,pop,prairie);
		
			
			

		
	}
	
}
